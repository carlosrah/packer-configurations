#!/bin/bash

echo "---Updating aptitude's repo's."
sudo apt-get update >> log

echo "---Upgrading Ubuntu to the latest of everything."
sudo apt-get upgrade -y --force-yes >> log

echo "---Installing Apache."
sudo apt-get install -y --force-yes apache2 >> log

echo "The End..."

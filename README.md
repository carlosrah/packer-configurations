Packer Configurations
=====================

Using packer to create machine images template quickly.

If you are just getting started with packer, please go to http://www.packer.io/docs

Current builders on this repo
=============================

**Virtual Box**
**DigitalOcean**


